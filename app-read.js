"use strict";

var assert = require("power-assert");
var fs = require("fs");
var JSON = require('JSON');
var opmlToJSON = require("opml-to-json");
var xml = fs.readFileSync(__dirname + "/resources/text/overcast.opml");

function app(){
	opmlToJSON(xml, function(error, json) {
		console.log(json);
	});
}

app();
