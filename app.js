// http://nodejs.org/api.html#_child_processes
var sys  = require('sys'),
    exec = require('child_process').exec,
    BASE = '~/projects/moneymachine';

function puts(error, stdout, stderr){
  sys.puts(stdout);
}

function app(){
  var macros = [],
      mask ='___jj_',
      hash = ';';

  macros.push('cd '+BASE);
  macros.push('echo "'+BASE+'"; echo');
     
  macros.push('php -f app.php -- "'+mask+'" "'+hash+'"');

  exec(macros.join('; '), puts);
}

app();
